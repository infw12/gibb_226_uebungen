package auftrag_3_arrays_und_referenzen_modulare_programmierung;

public class Leibniz {

	public static void main(String[] args) {
        double[] accuracies;
        double[][] results;

        // Filling the accuracies array, from 10^2 to 10^8
        accuracies = calculateAccuracies(2, 8);
        // Filling the results arary.
        results = calculateResults(accuracies);

        // Printing the calculated output.
        printOutput(results, accuracies);

        // Writing the calculated results to a file.
        CSVMagic.toCSV(results, "leibniz.csv");
        System.out.println("------------------------------------");

        // Reading the results from the file again.
        double[][] results2 = CSVMagic.fromCSV("leibniz.csv");
        // Printing them again. If all went well, the two outputs should be equivalent.
        printOutput(results2, accuracies);
	}

    private static double[] calculateAccuracies(int lowerExponent, int upperExponent) {
        // Assuming lowerExponent = 3, upperExponent = 6, we'll want to calculate it for the exponents 3,4,5 and 6,
        // a total of four exponents. That'd be 6 - 3 + 1.
        double[] accuracies = new double[upperExponent - lowerExponent + 1];
        int index = 0;

        for(int i=lowerExponent; i<upperExponent+1; i++) {
            accuracies[index] = Math.pow(10, i);
            index++;
        }

        return accuracies;
    }

    private static double[][] calculateResults(double[] accuracies) {
        double[][] results = new double[2][accuracies.length];

        for(int i=0; i < accuracies.length; i++) {
            double result = leibniz((int) accuracies[i]);
            double difference = Math.abs(Math.PI - result);
            results[0][i] = result;
            results[1][i] = difference;
        }

        return results;
    }

    private static void printOutput(double[][] results, double[] accuracies) {
        String headerString = String.format("%-16s%-24s%s", "Steps", "Result", "Difference");
        System.out.println(headerString);

        for(int i=0; i< results[0].length; i++) {
            // Formatting the output string to make it look nice.
            // %-16s: String, 16 chars wide, left-alinged. (So padding on the right).
            // %-24s: String, 24 chars wide, left-alinged. (So padding on the right).
            // %.12f: Float, 12 decimal places.
            String outputString = String.format("%-16s%-24s%.8f", ((Double)accuracies[i]).intValue(), results[0][i], results[1][i]);
            System.out.println(outputString);
        }
    }


	
	private static double leibniz(int accuracy) {
		double pi = 0;
		// Double because 1 / 3 would produce an int instead of a double.
		double denominator = 1;
		boolean add = true;
		
		for (int i=0; i< accuracy; i++) {
			if (add) {
				pi += 1 / denominator;
			}
			else {
				pi -= 1 / denominator;
			}
			
			add = !add;
			denominator += 2;
		}
		
		pi *= 4;
		
		return pi;
	}

}
