package auftrag_3_arrays_und_referenzen_modulare_programmierung;

import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

public class CSVMagic {

    public static void toCSV(double[][] results, String path) {

        try{
            // Creating a stream + writer, to write to a file.
            FileWriter fstream = new FileWriter(path);
            BufferedWriter out = new BufferedWriter(fstream);

            // Iterating over all items in the array, and writing them to the file.
            for (int i=0; i< results[0].length; i++) {
                out.write(results[0][i] + "," + results[1][i] + "\n");
            }
            //Close the output stream
            out.close();
        }
        catch (IOException e){
            System.err.println(e.getMessage());
        }
    }

    public static double[][] fromCSV(String path) {
        // Lists, unlike arrays, can be resized dynamically.
        List<Double> results = new ArrayList<Double>();
        List<Double> differences = new ArrayList<Double>();

        try {
            // Creating a stream & reader to read from a file.
            FileReader fstream = new FileReader(path);
            BufferedReader in = new BufferedReader(fstream);

            String line = null;
            // Reading lines until the end of the file is reached.
            while ((line = in.readLine()) != null) {
                // Splitting the incoming line, e.g. splitting "3.14,10" into ["3.14", "10"]
                String[] resultAndDifference = line.split(",");
                // Parsing the string into doubles, and appending them to the lists.
                results.add(Double.parseDouble(resultAndDifference[0]));
                differences.add(Double.parseDouble(resultAndDifference[1]));

            }
            in.close();
        }
        catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        // Creating a new two-dimensional array of type double.
        double[][] data = new double[2][results.size()];

        // Iterating over all elements in the 'results' and 'differences' list, and adding the pairs to the 'data' array.
        for (int i = 0; i < results.size(); i++) {
            data[0][i] = results.get(i);
            data[1][i] = differences.get(i);
        }

        return data;

    }

}
