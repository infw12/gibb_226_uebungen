package auftrag_3_arrays_und_referenzen_modulare_programmierung;

public class Zeugnisnoten {

	public static void main(String[] args) {
		double[] noten = new double[] {2.5, 5.5, 4.5, 6.0, 3.5, 4.5, 5.0, 4.0, 4.5, 5.0};
		
		CalculateThingies(noten);		
	}
	
	private static void CalculateThingies(double[] noten) {
		double minimum = noten[0];
		double maximum = noten[0];
		double summe = 0;
		
		double durchschnitt = 0;
		
		for (double note : noten) {
			summe += note;
			
			if (note < minimum) {
				minimum = note;
			}
			
			if (note > maximum) {
				maximum = note;
			}
			
		}
		
		durchschnitt = summe / noten.length;
		
		System.out.println("Schlechteste Note: " + minimum);
		System.out.println("Beste Note: " + maximum);
		System.out.println("Durchschnitt: " + durchschnitt);
	}

}
