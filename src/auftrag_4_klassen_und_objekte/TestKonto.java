package auftrag_4_klassen_und_objekte;

import java.util.UUID;

public class TestKonto {

    public static void main(String[] args) {

        Kunde kundeMichael = new Kunde("Michael", "Senn", UUID.randomUUID());
        Kunde kundeHans = new Kunde("Hans", "Müller", UUID.randomUUID());

        Konto kontoMichael = new Konto(0.01, kundeMichael);
        Konto kontoHans = new Konto(0.02, kundeHans);

        kontoMichael.deposit(10000);
        kontoHans.deposit(1000);

        kontoMichael.payInterest(365);
        kontoHans.payInterest(365 * 3);

        kontoMichael.setInterestRate(0.05);
        kontoMichael.payInterest(365);

        System.out.println(kontoMichael.prettyPrint());
        System.out.println("--------");
        System.out.println(kontoHans.prettyPrint());

    }


}
