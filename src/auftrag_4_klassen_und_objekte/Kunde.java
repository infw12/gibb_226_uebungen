package auftrag_4_klassen_und_objekte;

import java.util.UUID;

public class Kunde {
    private String name;
    private String firstName;
    private UUID customerID;


    public Kunde(String firstName, String name, UUID customerID) {

        this.firstName = firstName;
        this.name = name;
        this.customerID = customerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getName() {
        return name;
    }

    public UUID getCustomerID() {
        return customerID;
    }

    public String prettyPrint() {
        return String.format("%s %s (ID: %s)", firstName, name, customerID.toString());
    }


}
