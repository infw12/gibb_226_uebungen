package auftrag_4_klassen_und_objekte;

public class Konto {
    private double interestRate = 0.01;
    private double saldo = 0;

    private Kunde owner;

    public Konto(double interestRate, Kunde owner) {

        this.interestRate = interestRate;
        this.owner = owner;
    }

    public Kunde getOwner() {
        return owner;
    }

    public double getSaldo() {
        return saldo;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public void deposit(double amount) {
        saldo += Math.abs(amount);
    }

    public void payInterest(int lengthInDays) {
        double interest = saldo * interestRate * lengthInDays / 365;

        saldo += interest;
    }

    public String prettyPrint() {
        return String.format("Saldo: %s\nInterest Rate: %s%%\nOwner: %s", saldo, interestRate * 100, owner.prettyPrint());
    }
}
