package party;

public class PartyCore {
    private  static int numberOfGuests;

    public static void setNbOfGuests(int n) {

        numberOfGuests = Math.abs(n);
    }

    public static int cheers() {

        return numberOfGuests * (numberOfGuests - 1) / 2;
    }
}
