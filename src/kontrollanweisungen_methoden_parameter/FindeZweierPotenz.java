package kontrollanweisungen_methoden_parameter;

public class FindeZweierPotenz {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int Input = 15;
		int Test = 1;
		int Exponent = 0;
		
		
		while (Input > Test) {
			
			//Test *= 2;
			Test <<= 1;
			Exponent += 1;
		}
		
		System.out.println("The next power of two above " + Input + 
							" is " + Test + 
							" and has the exponent " + Exponent);

	}

}
