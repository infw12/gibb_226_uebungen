package kontrollanweisungen_methoden_parameter;

public class GGTNachEuklid {

	public static void main(String[] args) {
		int p = 120;
		int q = 36;

		if (p < q) {
			int temp = p;
			p = q;
			q = temp;
		}

		while (q != 0) {
			int r = p % q;
			p = q;
			q = r;
		}
		
		System.out.println(p);
		
	}

}
