package kontrollanweisungen_methoden_parameter;

public class SterncheenDreiecke {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		lazySolution();
	}
	
	private static void lazySolution() {
		int SternchenLineCount = 3;
		int SternchenCount = 1;
		
		for (int i = 0; i < SternchenLineCount; i++) {
			printSternchenLine(SternchenCount);
			SternchenCount++;
		}
	}
	
	private static void mathsSolution() {
//		Print X asterisks
		
	}
	
	private static void printSternchenLine(int Count) {
		
		for (int i = 0; i < Count; i++) {
			System.out.print("*");
		}
		System.out.print("\n");
	}

}
