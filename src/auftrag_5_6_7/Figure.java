package auftrag_5_6_7;

import java.awt.Color;
import java.awt.Point;
import java.awt.Graphics;

public class Figure {
    protected Color colour = new Color(0, 0, 0);
    protected Point position = new Point(0, 0);

    public Figure() {

    }

    public Figure(Color colour, Point position) {
        this.colour = colour;
        this.position = position;
    }

    public void move(int dx, int dy) {
        this.position.x += dx;
        this.position.y += dy;
    }

    public void draw(Graphics g) {

    }

    public String prettyPrint() {
        return String.format("Figure: \n  Position: x=%s y=%s", position.x, position.y);
    }

    public String toCSV() {
        return String.format("%s\t%s,%s,%s\t%s,%s",
                this.getClass().getCanonicalName(),
                this.colour.getRed(),
                this.colour.getGreen(),
                this.colour.getBlue(),
                this.position.getX(),
                this.position.getY());
    }

    public void fromCSV(String line) {
        // line should be something like: Figure	130,75,245	15.0,20.0

        String[] thingies = line.split("\t");

        String[] rgb = thingies[1].split(",");
        int red = Integer.valueOf(rgb[0]);
        int green = Integer.valueOf(rgb[1]);
        int blue = Integer.valueOf(rgb[2]);

        String[] xy = thingies[2].split(",");
        int x = Double.valueOf(xy[0]).intValue();
        int y = Double.valueOf(xy[1]).intValue();

        this.colour = new Color(red, green, blue);
        this.position = new Point(x, y);
    }

}
