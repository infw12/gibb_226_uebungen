package auftrag_5_6_7;

import java.awt.Color;
import java.awt.Point;

import java.util.ArrayList;
import java.util.List;

public class Editor {

    public static void main(String[] args) {

        //aufgabe1();
        //aufgabe2();
        //aufgabe3();
//        aufgabe4();
        //aufgabe5();
//        aufgabe6();
        aufgabe7_2();
    }

    private static void aufgabe1() {
        Color colour = new Color(131,242,0);
        Point position = new Point(0, 0);

        Rectangle r1 = new Rectangle(colour, position, 10, 10);
        Rectangle r2;

        Circle c1 = new Circle(colour, position, 10);

        Figure f1;

        //Rechteckobjekt Rechteckreferenz zuweisen.
        r2 = new Rectangle(colour, position, 10, 10);
        r2 = r1;

        //Kreisobjekt Rechteckreferenz zuweisen.
        //r2 = new Circle(colour, position, 10);
        //r2 = c1;

        // Rechteckobjekt Figurreferenz zuweisen.
        f1 = new Rectangle(colour, position, 10, 10);
        f1 = r1;

        // Kreisobjekt Figurreferenz zuweisen.
        f1 = new Circle(colour, position, 10);
        f1 = c1;
    }

    private static void aufgabe2() {
        Color colour = new Color(131,242,0);
        Point position = new Point(0, 0);

        Figure f1;

        // Mit Figurreferenz (die auf ein Rechteck zeigt) auf:
        f1 = new Rectangle(colour, position, 10, 10);
        // eine Instanzvariable der 'Figure' Klasse zugreifen.
        System.out.println("f1.colour = " + f1.colour);
        // eine Instanzvariable der 'Rectangle' Klasse zugreifen.
        //System.out.println("f1.height = " + f1.height);
    }

    private static void aufgabe3() {
        Color colour = new Color(131,242,0);
        Point position = new Point(0, 0);
        Point end = new Point(10, 25);

        Line l = new Line(colour, position, end);
        System.out.println(l.prettyPrint());
        l.move(10, 5);
        System.out.println(l.prettyPrint());

    }

    private static void aufgabe4() {
        Line line = new Line(new Color(255,255,255), new Point(10, 20), new Point(30, 40));
        Figure figure = new Line(new Color(255, 255, 255), new Point(10, 20), new Point(30, 40));

        System.out.println(line.prettyPrint());
        System.out.println(figure.prettyPrint());

        line.move(10, 10);
        figure.move(10, 10);

        System.out.println(line.prettyPrint());
        System.out.println(figure.prettyPrint());

    }

    private static void aufgabe5() {
        String CSVPath = "figures.csv";

        List<Figure> figures = new ArrayList<Figure>();

        Point point1 = new Point(10, 30);
        Point point2 = new Point(25, 5);
        Point point3 = new Point(15, 20);

        Color colour1 = new Color(125, 250, 55);
        Color colour2 = new Color(130, 75, 245);
        Color colour3 = new Color(15, 170, 110);

        Figure f = new Figure(colour2, point3);
        Circle c = new Circle(colour1, point2, 10);
        Rectangle r = new Rectangle(colour3, point1, 5, 10);
        Line l = new Line(colour2, point3, point1);

        figures.add(f);
        figures.add(c);
        figures.add(r);
        figures.add(l);

        System.out.println(String.format("The following figures will be written to %s.", CSVPath));
        for (Figure figure : figures) {
            System.out.println(figure.prettyPrint());
        }

        SaveManager.writeToCSV(figures, CSVPath);
        System.out.println(String.format("Figures written to %s", CSVPath));

        figures = SaveManager.loadFromCSV(CSVPath);
        System.out.println(String.format("Figures read from %s", CSVPath));

        for (Figure figure : figures) {
            System.out.println(figure.prettyPrint());
        }
    }

    private static void aufgabe6() {

        Color colour1 = new Color(125, 250, 55);
        Color colour2 = new Color(130, 75, 245);
        Color colour3 = new Color(15, 170, 110);
        List<Point> pointList = new ArrayList<Point>();
        pointList.add(new Point(0, 0));
        pointList.add(new Point(100, 10));
        pointList.add(new Point(100, 500));
        pointList.add(new Point(350, 450));

        List<Figure> figures = new ArrayList<Figure>();
//
//        figures.add(new Rectangle(colour1, new Point(150, 150), 70, 210));
//        figures.add(new Circle(colour3, new Point(390, 250), 130));
//        figures.add(new Line(colour2, new Point(210, 610), new Point(50, 50)));
        figures.add(new Polygon(colour2, new Point(123, 123), pointList));

        Drawing drawing = new Drawing(figures);

        Zeichenbrett drawingBoard = new Zeichenbrett();

        while (true) {
            drawingBoard.zeige(drawing);

            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                System.err.println(e);
            }

//            drawing.move(5, 3);
//            drawing.party(5);
        }

    }

    private static void aufgabe7_2() {
        Drawing drawing = new Drawing();
        System.out.println("Starting up");
        Zeichenbrett drawingBoard = new Zeichenbrett();
        drawingBoard.zeige(drawing);

    }
}
