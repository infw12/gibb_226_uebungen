package auftrag_5_6_7;

import java.awt.Point;
import java.awt.Color;
import java.awt.Graphics;

import java.util.List;

public class Polygon extends Figure {
    private List<Point> points;

    public Polygon(Color colour, Point position, List<Point> points) {
        super(colour, position);
        this.points = points;
    }

    @Override
    public void move(int dx, int dy) {
        super.move(dx, dy);

        for (Point p : points) {
            p.x += dx;
            p.y += dy;
        }
    }


    public void draw(Graphics g){
        // Todo: Draw final line.
        g.setColor(this.colour);
        Point oldPoint = position;

        for (Point p : points) {
            g.drawLine(oldPoint.x, oldPoint.y, p.x, p.y);
            oldPoint = p;
        }

        // Drawing the line between the first and the last point.
        if (!points.isEmpty()) {
            Point lastPoint = points.get(points.size() - 1);
            g.drawLine(position.x, position.y, lastPoint.x, lastPoint.y);
        }


    }

    public String prettyPrint() {
// Todo
//        return String.format("Polygon: \n  Point1: x=%s y=%s\n  Points: x=%s y=%s", position.x, position.y, end.x, end.y);
        return "Polygon";
    }

    public String toCSV() {
//        Todo
//        return String.format("%s\t%s,%s,%s\t%s,%s\t%s,%s",
//                this.getClass().getCanonicalName(),
//                this.colour.getRed(),
//                this.colour.getGreen(),
//                this.colour.getBlue(),
//                this.position.getX(),
//                this.position.getY(),
//                this.end.getX(),
//                this.end.getY());
        return "Hai";
    }

    public void fromCSV(String line) {
        // line should be something like: Line	130,75,245	15.0,20.0	10.0,30.0

//        Todo
//        String[] thingies = line.split("\t");
//
//        super.fromCSV(line);
//
//        String[] xy2 = thingies[3].split(",");
//        int x2 = Double.valueOf(xy2[0]).intValue();
//        int y2 = Double.valueOf(xy2[1]).intValue();
//
//        this.end = new Point(x2, y2);
    }

}
