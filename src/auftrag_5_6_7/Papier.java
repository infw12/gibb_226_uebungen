package auftrag_5_6_7;

import java.awt.Graphics;

import java.awt.Color;
import java.awt.Point;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;

/**
 * Papier ist eine von JPanel abgeleitete Swing-Komponente.
 * - Sie dient als "Zeichenblatt" f�r Zeichnungen.
 * - Sie speichert die Zeichnung in einer Instanzvariablen.
 * - Sie �berschreibt die Methode paintComponent der Klasse JPanel
 *   so, dass bei ihrem Aufruf die Zeichnung auf das Papier gezeichnet
 *   wird.
 * 
 * @author Andres Scheidegger
 */
public class Papier extends JPanel implements MouseListener {
	private int x, y;
    public String figure = "r";
	private Drawing zeichnung;

    public Papier() {
        addMouseListener(this);
    }

	@Override
	public void paintComponent(Graphics g) {
		if (zeichnung != null) {
			zeichnung.draw(g);
		}
	}
	

	public void setZeichnung(Drawing zeichnung) {
		this.zeichnung = zeichnung;
	}
    @Override
    public void mousePressed(MouseEvent e) {

        x = e.getX();
        y = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        Color color =  new Color(0, 0, 0);

        int x2 = e.getX();
        int y2 = e.getY();

        if (figure.equals("r")) {
            int xPosition = x > x2 ? x2 : x;
            int yPosition = y > y2 ? y2 : y;

            int breadth = Math.abs(x2 - x);
            int height = Math.abs(y2 - y);

            Rectangle r = new Rectangle(color, new Point(xPosition, yPosition), breadth, height);
            zeichnung.figures.add(r);
        }
        else if (figure.equals("l")) {
            Line l = new Line(color, new Point(x, y), new Point(x2, y2));
            zeichnung.figures.add(l);
        }
        else if (figure.equals("k")) {
            int deltaX = Math.abs(x2 - x);
            int deltaY = Math.abs(y2 - y);
            // Pythagoras
            int radius = (int)Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY,  2));

            Circle c = new Circle(color, new Point(x, y), radius);
            zeichnung.figures.add(c);
        }


        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}


}
