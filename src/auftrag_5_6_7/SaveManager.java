package auftrag_5_6_7;

import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

public class SaveManager {

    public static void writeToCSV(List<Figure> figures, String path) {

        try{
            // Creating a stream + writer, to write to a file.
            FileWriter fstream = new FileWriter(path);
            BufferedWriter out = new BufferedWriter(fstream);

            // Iterating over all items in the array, and writing them to the file.
            for (int i=0; i < figures.size(); i++) {
                out.write(figures.get(i).toCSV());
                if (i != figures.size() - 1) {
                    out.write("\n");
                }
            }

            //Close the output stream
            out.close();
        }
        catch (IOException e){
            System.err.println(e.getMessage());
        }
    }

    public static List<Figure> loadFromCSV(String path) {
        // Lists, unlike arrays, can be resized dynamically.
        List<Figure> figures = new ArrayList<Figure>();

        try {
            // Creating a stream & reader to read from a file.
            FileReader fstream = new FileReader(path);
            BufferedReader in = new BufferedReader(fstream);

            String line = null;
            // Reading lines until the end of the file is reached.
            while ((line = in.readLine()) != null) {
                figures.add(parseCSVLine(line));
            }
            in.close();
        }
        catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        return figures;

    }

    public static Figure parseCSVLine(String line) {
        /** We'll get lines looking something like:
         * Figure	130,75,245	15.0,20.0
         * Circle	125,250,55	25.0,5.0	10
         * Rectangle	15,170,110	10.0,30.0	5	10
         * The first entry when splitting by \t will be the class, depending on which we delegate the parsing to some other class' fromCSV() function.
         */

        String className = line.split("\t")[0];
        Figure figure;
        try {
            figure = (Figure)Class.forName(className).getConstructor().newInstance();
            figure.fromCSV(line);
            return figure;
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;


//
//        // Switch would be prettier, but only works with Java >=1.7. Not worth breaking compatability for. :)
//        if (className.equals("Circle")) {
//            return Circle.fromCSV(line);
//        } else if (className.equals("Line")) {
//            return Line.fromCSV(line);
//        } else if (className.equals("Rectangle")) {
//            return Rectangle.fromCSV(line);
//        } else {
//            return Figure.fromCSV(line);
//        }
    }
}
