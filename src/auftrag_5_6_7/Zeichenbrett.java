package auftrag_5_6_7;

import javax.swing.JFrame;

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

/**
 * Die Klasse Zeichenbrett erm�glicht die Darstellung von Zeichnungen.
 * Sie �ffnet ein Fenster auf dem Desktop, welches als einzige Komponente
 * ein Papierobjekt enth�lt.
 * 
 * @author Andres Scheidegger
 */
public class Zeichenbrett extends JFrame implements KeyListener{

	private Papier papier = new Papier();
	

	public Zeichenbrett() {
		super("Zeichenbrett f�r Figuren");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		setLocation(200, 200);
		setContentPane(papier);
		setVisible(true);

        addKeyListener(this);
    }

	public void zeige(Drawing zeichnung) {
		papier.setZeichnung(zeichnung);
		// Aufruf von repaint() bewirkt, dass das Fenster des Zeichenbrett
		// auf dem Dektop neu aufgebaut wird.
		repaint();
	}

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        papier.figure = Character.toString(e.getKeyChar());
    }

    @Override
    public void keyReleased(KeyEvent e) {}
}
