package auftrag_5_6_7;

import java.awt.Color;
import java.awt.Point;
import java.awt.Graphics;

public class Rectangle extends Figure {
    private int breadth;
    private int height;

    public Rectangle() {

    }

    public Rectangle(Color colour, Point position, int breadth, int height) {
        super(colour, position);
        this.breadth = breadth;
        this.height = height;
    }


    public void draw(Graphics g) {
        g.setColor(this.colour);
        g.drawRect(position.x, position.y, breadth, height);
    }

    public String prettyPrint() {
        return String.format("Rectangle: \n  Position: x=%s y=%s\n  Breadth: %s\n  Height: %s", position.x, position.y, breadth, height);
    }

    public String toCSV() {
        return String.format("%s\t%s,%s,%s\t%s,%s\t%s\t%s",
                this.getClass().getCanonicalName(),
                this.colour.getRed(),
                this.colour.getGreen(),
                this.colour.getBlue(),
                this.position.getX(),
                this.position.getY(),
                this.breadth,
                this.height);
    }

    public void fromCSV(String line) {
        // line should be something like: Rectangle	15,170,110	10.0,30.0	5	10

        String[] thingies = line.split("\t");

        super.fromCSV(line);

        int breadth = Double.valueOf(thingies[3]).intValue();
        int height = Double.valueOf(thingies[4]).intValue();

        this.breadth = breadth;
        this.height = height;


    }

}