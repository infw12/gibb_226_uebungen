package auftrag_5_6_7;

import java.awt.Color;
import java.awt.Point;
import java.awt.Graphics;

public class Circle extends Figure {
    private int radius;

    public Circle() {

    }

    public Circle(Color colour, Point position, int radius) {
        super(colour, position);

        this.radius = radius;
    }


    public void draw(Graphics g) {
        g.setColor(this.colour);
        g.drawOval(position.x - radius, position.y - radius, radius*2, radius*2);
    }

    public String prettyPrint() {
        return String.format("Circle: \n  Midpoint: x=%s y=%s\n  Radius: %s", position.x, position.y, radius);
    }

    public String toCSV() {
        return String.format("%s\t%s,%s,%s\t%s,%s\t%s",
                this.getClass().getCanonicalName(),
                this.colour.getRed(),
                this.colour.getGreen(),
                this.colour.getBlue(),
                this.position.getX(),
                this.position.getY(),
                this.radius);
    }

    public void fromCSV(String line) {
        // line should be something like: Circle	125,250,55	25.0,5.0	10

        String[] thingies = line.split("\t");

        super.fromCSV(line);

        int radius = Integer.valueOf(thingies[3]);

        this.radius = radius;
    }

}
