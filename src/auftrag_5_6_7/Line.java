package auftrag_5_6_7;

import java.awt.Point;
import java.awt.Color;
import java.awt.Graphics;

public class Line extends Figure {
    private Point end;

    public Line() {

    }

    public Line(Color colour, Point position, Point end) {
        super(colour, position);
        this.end = end;
    }

    // Since a line is defined by two points, it'll need to handle its moving itself, since the Figure class only works for those with one point.
    @Override
    public void move(int dx, int dy) {
        super.move(dx, dy);

        this.end.x += dx;
        this.end.y += dy;
    }


    public void draw(Graphics g){
        g.setColor(this.colour);
        g.drawLine(this.position.x, this.position.y, this.end.x, this.end.y);
    }

    public String prettyPrint() {
        return String.format("Line: \n  Point1: x=%s y=%s\n  Point2: x=%s y=%s", position.x, position.y, end.x, end.y);
    }

    public String toCSV() {
        return String.format("%s\t%s,%s,%s\t%s,%s\t%s,%s",
                this.getClass().getCanonicalName(),
                this.colour.getRed(),
                this.colour.getGreen(),
                this.colour.getBlue(),
                this.position.getX(),
                this.position.getY(),
                this.end.getX(),
                this.end.getY());
    }

    public void fromCSV(String line) {
        // line should be something like: Line	130,75,245	15.0,20.0	10.0,30.0

        String[] thingies = line.split("\t");

        super.fromCSV(line);

        String[] xy2 = thingies[3].split(",");
        int x2 = Double.valueOf(xy2[0]).intValue();
        int y2 = Double.valueOf(xy2[1]).intValue();

        this.end = new Point(x2, y2);
    }
}
