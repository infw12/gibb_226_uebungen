package auftrag_5_6_7;

import java.awt.Graphics;
import java.util.List;
import java.util.ArrayList;

import java.util.Random;

public class Drawing {
    protected List<Figure> figures;
    private Random generator;

    public Drawing() {
        this.figures = new ArrayList<Figure>();
    }

    public Drawing(List<Figure> figures) {

        this.figures = figures;
        generator = new Random();
    }

    public void draw(Graphics g) {

        for (Figure f : this.figures) {
            f.draw(g);
        }
    }

    public void move(int dx, int dy) {

        for (Figure f : figures) {
            f.move(dx, dy);
        }
    }

    public void party(int maxMovement) {

        for (Figure f : figures) {
            int dx = generator.nextInt(maxMovement + 1);
            int dy = generator.nextInt(maxMovement + 1);

            if (generator.nextBoolean()) {
                dx *= -1;
            }

            if (generator.nextBoolean()) {
                dy *= -1;
            }

            f.move(dx, dy);
        }
    }

}
